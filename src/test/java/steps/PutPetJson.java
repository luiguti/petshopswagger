package steps;

import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.Assert;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

public class PutPetJson {
    @When("I put a pet with Json ([^\"]*)")
    public void iPutAPetWithJson(String jsonroute) {
        RestAssured.baseURI = "https://petstore.swagger.io/v2";
        RequestSpecification request = RestAssured.given();
        String route = "src/test/resources/jsonfiles/";
        route = route.concat(jsonroute);
        route = route.concat(".json");
        JSONParser parser = new JSONParser();
        JSONObject jsonObject = null;
        try (Reader reader = new FileReader(route)) {
            jsonObject = (JSONObject) parser.parse(reader);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        request.header("Content-Type", "application/json");
        request.body(jsonObject.toJSONString());
        Response response = request.put("/pet");
        int statusCode = response.getStatusCode();
        Assert.assertEquals(200, statusCode);
    }
}
