package steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.Assert;

import java.io.*;

public class PetJson {
    @When("I post a new pet with Json ([^\"]*)")
    public void iPostANewPetWithJson(String jsonroute) {
        RestAssured.baseURI = "https://petstore.swagger.io/v2";
        RequestSpecification request = RestAssured.given();
        String route = "src/test/resources/jsonfiles/";
        route = route.concat(jsonroute);
        route = route.concat(".json");
        JSONParser parser = new JSONParser();
        JSONObject jsonObject = null;
        try (Reader reader = new FileReader(route)) {
            jsonObject = (JSONObject) parser.parse(reader);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        request.header("Content-Type", "application/json");
        request.body(jsonObject.toJSONString());
        Response response = request.post("/pet");
        int statusCode = response.getStatusCode();
        Assert.assertEquals(200, statusCode);
    }

    @Then("I should look for that pet ([^\"]*)")
    public void iShouldLookForThatPet(String jsonroute) {
        //Deberia hacer un get de la mascota que acabo de POSTear para ver si se ha enviado
        String route = "src/test/resources/jsonfiles/";
        route = route.concat(jsonroute);
        route = route.concat(".json");
        JSONParser parser = new JSONParser();
        JSONObject jsonObject = null;
        try (Reader reader = new FileReader(route)) {
            jsonObject = (JSONObject) parser.parse(reader);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String path = "https://petstore.swagger.io/v2/pet/";
        String petID = String.valueOf(jsonObject.get("id"));
        path = path.concat(petID);
        Response response = RestAssured.get(path);

        BufferedWriter writer = null;
        String pathname = "src/test/resources/results/result";
        pathname = pathname.concat(jsonroute);
        pathname = pathname.concat(".json");
        try {
            File logFile = new File(pathname);
            writer = new BufferedWriter(new FileWriter(logFile));
            writer.write(response.getBody().asString());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                writer.close();
            } catch (Exception e) {
            }
        }
    }

    @And("The json that i get is the same that I have in ([^\"]*)")
    public void theJsonThatIGetIsTheSameThatIHaveInPetJson(String jsonroute) {
        //tengo la pet recien POSTeada en local. Compruebo que dicha pet y la que tenia por argumento
        //coinciden (mismo nombre)
        String route = "src/test/resources/jsonfiles/";
        route = route.concat(jsonroute);
        route = route.concat(".json");
        JSONParser parser = new JSONParser();
        String nameLocal = null;
        String name = null;
        try (Reader reader = new FileReader(route)) {
            JSONObject jsonObject = (JSONObject) parser.parse(reader);
            nameLocal = (String) jsonObject.get("name");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String pathname = "src/test/resources/results/result";
        pathname = pathname.concat(jsonroute);
        pathname = pathname.concat(".json");
        try (Reader reader = new FileReader(pathname)) {
            JSONObject jsonObject = (JSONObject) parser.parse(reader);
            name = (String) jsonObject.get("name");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Assert.assertEquals("Comprobacion nombre mascota obtenida: ", nameLocal, name);
    }
}
