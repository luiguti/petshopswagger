package steps;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.Assert;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

public class DeletePetJson {
    @When("I request to delete a pet by ([^\"]*)")
    public void iRequestToDeleteAPetById(String id) {
        RestAssured.baseURI = "https://petstore.swagger.io/v2";
        RequestSpecification request = RestAssured.given();
        request.header("Content-Type", "application/json");
        String toDelete = "/pet/";
        toDelete = toDelete.concat(id);
        Response response = request.delete(toDelete);
        int statusCode = response.getStatusCode();
        Assert.assertEquals(200, statusCode);
    }

    @Then("I check if that pet still exists ([^\"]*)")
    public void iCheckIfThatPetStillExists(String id) {
        String request = "https://petstore.swagger.io/v2/pet/";
        request = request.concat(id);
        Response response = RestAssured.get(request);
        int statusCode = response.getStatusCode();
        Assert.assertEquals(404, statusCode);
    }
}