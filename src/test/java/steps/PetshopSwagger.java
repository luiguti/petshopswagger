package steps;


import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.json.simple.JSONObject;
import org.junit.Assert;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class PetshopSwagger {
    private static int code = 0;
    @When("I request to get a user by id ([^\"]*)")
    public void iRequestToGetAUserByIdUserID(String id) {
        String request = "https://petstore.swagger.io/v2/pet/";
        request = request.concat(id);
        Assert.assertEquals("https://petstore.swagger.io/v2/pet/3456789876544335670", request);
        Response response = RestAssured.get(request);
        //String a = Integer.toString(response.getStatusCode());
        BufferedWriter writer = null;

        try {
            File logFile = new File("src/test/resources/results.json");
            writer = new BufferedWriter(new FileWriter(logFile));

            writer.write(response.getBody().asString());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                writer.close();
            } catch (Exception e) {
            }
        }
        /*try {
            File logFile = new File("src/test/resources/code.txt");
            writer = new BufferedWriter(new FileWriter(logFile));
            writer.write(a);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                writer.close();
            } catch (Exception e) {
            }
        }*/
        code = response.getStatusCode();

    }

    @Then("I should get expected code ([^\"]*)")
    public void iShouldGetExpectedCodeExpectedCode(int expectedCode) {
        /*String givenresponse = null;
        try {
            // FileReader reads text files in the default encoding.
            FileReader fileReader =
                    new FileReader("src/test/resources/code.txt");

            // Always wrap FileReader in BufferedReader.
            BufferedReader bufferedReader =
                    new BufferedReader(fileReader);

            givenresponse = bufferedReader.readLine();

            // Always close files.
            bufferedReader.close();
        }   //reader
        catch (FileNotFoundException ex) {
            System.out.println(
                    "Unable to open file '" +
                            "results" + "'");
        } catch (IOException ex) {
            System.out.println(
                    "Error reading file '"
                            + "results" + "'");
        }*/
        Assert.assertEquals(expectedCode, code);
    }

    @And("The value for ([^\"]*) after the get operation should be ([^\"]*)")
    public void theValueForParameterAfterTheGetOperationShouldBeParameterValue(String parameter, String parametervalue) {
        JSONParser parser = new JSONParser();
        String name = null;
        try (Reader reader = new FileReader("src/test/resources/results.json")) {

            JSONObject jsonObject = (JSONObject) parser.parse(reader);

            name = (String) jsonObject.get(parameter);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Assert.assertEquals("Comprobacion nombre mascota obtenida: ", parametervalue, name);
    }
}
