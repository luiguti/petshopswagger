import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;


@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features = {"src/test/resources/features"},
        glue = {"steps"},

        plugin = {"pretty", "html:target/cucumber", "json:target/cucumber-report.json"}
        /*,
        plugin = { "pretty", "html:target/cucumber-html-reports",
                "json:target/cucumber-html-reports/cucumber.json","rerun:target/failed_scenarios.txt" },
        monochrome = true*/
)
public class TestRunner {
}