#!groovy
pipeline {
    agent any
    stages {
        stage('pull') {
            steps {
                git(
                        url: 'https://luiguti@bitbucket.org/luiguti/petshopswagger.git',
                        branch: 'TaggedBuild'
                )
                //sh "ls -l"
            }
        }
        stage('Unit & Integration Tests') {
            steps {
//                bat 'dir'
//                bat 'dir src\\test\\resources'
//                bat 'cd src'
//                bat 'dir'
                script {
                    def file = readFile('src\\test\\resources\\tags.csv')
                    String archivo = file as String
                    archivo = archivo.replace("\n",",")
                    bat 'gradlew -Dcucumber.options="--tags ' + archivo + '" clean test aggregate' //run a gradle task
                    //esta forma de pasar las tags funciona. Ahora deberia intentar leerlas de un csv
                }


            }
        }
        /*stage('emailing') {
            steps {
                emailext to: params.email, subject: "Jenkins Build ${currentBuild.currentResult}: Job ${env.JOB_NAME}",
                        body: "${currentBuild.currentResult}: Job ${env.JOB_NAME} build ${env.BUILD_NUMBER}\n More info at: ${env.BUILD_URL}"
                timeout(time: 60, unit: 'SECONDS') {
                    // ...
                }
            }
        }*/
        /*stage('sonar') {
            steps {
                bat 'gradlew sonarqube \
                -Dsonar.projectKey=petshopswagger \
                -Dsonar.host.url=http://localhost:9000 \
                -Dsonar.login=75a9034ce8a4ad0bf982cede47a0d3a9b154c197'
            }
        }*/
        /*stage('start child') {
            steps {
                parallel(
                        "firstTask": {
                            build job: "child", propagate: false
                        },
                        "secondTask": {
                            build job: "child", propagate: false
                        },
                        "thirdTask": {
                            build job: "child", propagate: false
                        }
                )
            }
        }*/
        /*stage('Parallel') {
            parallel {
                stage('Execute children in parallel') {
                    steps {
                        echo 'Parallel...'
                        echo params.email
                        echo params.numChildren
                        echo params.stringToPrint
                        script {
                            try {
                                int children = Integer.valueOf(params.numChildren)
                                for (int i = 0; i < children; i++) {
                                    build job: 'child', parameters: [[$class: 'StringParameterValue', name: 'stringToPrint', value: params.stringToPrint],
                                                                     [$class: 'StringParameterValue', name: 'numHijo', value: i.toString()]]

                                }
                            } catch (Exception e) {
                                echo "Parallel Caught: ${e}"
                            }
                        }
                    }
                }
                stage('Execute children in parallel 2') {
                    steps {
                        echo 'Parallel...'
                        echo params.email
                        echo params.numChildren
                        echo params.stringToPrint
                        script {
                            try {
                                int children = Integer.valueOf(params.numChildren)
                                for (int i = 0; i < children; i++) {
                                    build job: 'child', parameters: [[$class: 'StringParameterValue', name: 'stringToPrint', value: params.stringToPrint],
                                                                     [$class: 'StringParameterValue', name: 'numHijo', value: i.toString()]]

                                }
                            } catch (Exception e) {
                                echo "Parallel Caught: ${e}"
                            }
                        }
                    }
                }
            }
        }*/
    }
}
