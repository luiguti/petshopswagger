Feature: Petshop

  @Post
  Scenario Outline: Post a new pet into the store
    When I post a new pet with Json <petJson>
    Then I should look for that pet <petJson>
    And The json that i get is the same that I have in <petJson>
    Examples:
      | petJson |
      | pet1    |

  @Get
  Scenario Outline: Get an existing pet in the store
    When I request to get a user by id <userID>
    Then I should get expected code <expectedCode>
    And  The value for <parameter> after the get operation should be <parameterValue>
    Examples:
      | userID              | expectedCode | parameter | parameterValue |
      | 3456789876544335670 | 200          | name      | candy          |


  @Put
  Scenario Outline: Put a pet on the store
    When I put a pet with Json <petJson>
    Then I should look for that pet <petJson>
    And The json that i get is the same that I have in <petJson>
    Examples:
      | petJson |
      | pet1    |
      | pet2    |

  @Delete
  Scenario Outline: Delete a pet from the store
    When I request to delete a pet by <id>
    Then I check if that pet still exists <id>
    Examples:
      | id                  |
      | 3456789876544335670 |
      | 3456789876544335678 |